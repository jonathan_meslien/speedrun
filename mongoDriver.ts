import {forge_response} from "./response";
import {Document} from "mongoose";
import {rejected} from "./core";

interface MongooseDeleteResponse {
    deletedCount: number,
    [index: string]: any
}

interface MongooseUpdateResponse {
    n: number,
    nModified: number,
    [index: string]: any
}
// xport declare const execute_query: <O extends Pagination, T, F>(plan: QuerySchemaFunction<T, F>) => (execute_query_args: executeQueryArgs<O, T, F>) => Promise<any>;
export const execute_query = async (query: Function, onSuccess: Function, onFailure: Function) => {
  try {
    const documents = await query();
    return onSuccess(documents);
  } catch(e) {
    return onFailure(e);
  }
};

export const mongo_error = () => {
  return rejected(forge_response(500, 'server error', { error: "Mongo error" }));
}


export const mongo_success = (behavior: Function) => (documents: Document[]) => {
  return behavior(documents);
}

export const is_not_updated = (document: MongooseUpdateResponse) => {
  return document.n === 0;
}

export const is_not_deleted = (document: MongooseDeleteResponse) => {
  return document.deletedCount === 0;
}

export const is_not_found = (documents: Document[]) => {
  return documents.length === 0;
}

export const extract_documents = (documents: Document[]) => {
  return documents.map(document => document.toObject());
};

