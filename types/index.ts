import { SpeedRunRequest } from "../interfaces";

declare global{
  namespace Express {
    interface Request {
     speedRun: SpeedRunRequest
    }
  }
}
