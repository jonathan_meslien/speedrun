import axios from 'axios';

interface ApiCallConfig {
  url: string,
  method: "GET" | "POST" | "DELETE" | "UPDATE" | any,
  data: { [key: string] : any },
  accessToken?: string,
  headers?: { [key: string] : any },
}

interface SpeedRunApiResponse {
  successful: boolean,
  message: string, 
  data: any
}

export const execute_request = async (api_call_config: ApiCallConfig, onSucess: Function, onFailure: Function) => {
    const {
          url,
          method,
          data,
          accessToken,
          headers
      } = api_call_config;

      const dataOrParams = ["GET", "DELETE"].includes(method) ? "params" : "data";

      if (accessToken) {
          axios.defaults.headers.common["Authorization"]= `Bearer ${accessToken}`;
      }

      try {
        const response = await axios.request({ url, method, headers, [dataOrParams]: data })
        return onSucess(response);
      } catch (e) {
        return onFailure(e);
      }
}


export const api_success = (behavior: Function) => (data: SpeedRunApiResponse ) => {
  return behavior(data);
}


export const api_failed = (behavior: Function) => (data: any) => {
  return behavior(data);
}
