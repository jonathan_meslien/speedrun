import { http_code } from "./httpCode";
import express from 'express';
import { _ } from "./operator";
import { SpeedRun } from "./interfaces";

interface SpeedRunResponse {
  code: http_code,
  message: string,
  data: any,
  pagination?: { before: string, after: string }
}

export const forge_response = (code: http_code, message: string, payload: any): SpeedRunResponse => {
  if (payload.code && payload.message && payload.payload) {
    return payload;
  }

  return { code, message, data: payload };
}

const is_valid = (speedrun_response: SpeedRunResponse) => {
    if (!speedrun_response.code || !speedrun_response.message || !speedrun_response.data) 
        return false;
    return true;
}

const create_response = (message: string, payload: any) => {
  if (payload.hasOwnProperty('pagination')) {
    return {
      successful: true,
      message: message,
      data: payload.data,
      pagination: payload.pagination
    };
  } else {
    return {
      successful: true,
      message: message,
      data: payload
    };
  }
}

const handle_reponse = <T>(res: express.Response, code: http_code, message: string, speedrun: SpeedRun<T>) => (payload: any) => {
  const logger = speedrun.logger;
  logger.info("send response >> succeeded : >> " + code);
  logger.info('send response >> -------------------');

  res.status(code);
  res.json(create_response(message, payload));
}

const handle_response_error = <T>(res: express.Response, speedrun: SpeedRun<T>) => (speedrun_response: SpeedRunResponse) => {
    const logger = speedrun.logger;

    if (!is_valid(speedrun_response)) {
      logger.error("send response >> fatal error >> " + speedrun_response);
      logger.info('send response >> -------------------');
      res.status(500);
      res.json({
          successful: false,
          message: 'server error',
          data: { error: "1919" } 
      })
      return;
    }
  
    logger.error("send response >> failed  >> " + speedrun_response.code + " >> " + JSON.stringify(speedrun_response.data) );
    logger.info('send response >> -------------------');
    res.status(speedrun_response.code);
    res.json({
        successful: false,
        message: speedrun_response.message,
        data: speedrun_response.data 
    })
};

export const send_response = (res: express.Response, code: http_code, message: string)  => _(<O>(speedrun: SpeedRun<O>) => handle_reponse(res, code, message, speedrun), <O>(speedrun: SpeedRun<O>) => handle_response_error(res, speedrun));

