import mongoose from "mongoose";
import { SpeedRun } from '../interfaces'; 
type MongoMethod = "FIND" | "UPDATE" | "DELETE" | "CREATE";

export type QuerySchemaFunction<T,F> = (args: QuerySchemaArgs<T,F>) => QuerySchema;

export interface Resource {
  _id: string,
  [key: string]: any
}

export interface QuerySchemaArgs<T,F> {
  condition: T,
  fields?: F
}


export interface QuerySchema {
  name: string,
  identity_field: string,
  forbidden_fields: string[];
  model: any,
  condition: {},
  method: MongoMethod,
  fields?: {
    [key: string]: any
  },
  unprotected: boolean;
}

export interface MongooseDeleteResponse {
    deletedCount: number,
    [index: string]: any
}

export interface MongooseUpdateResponse {
    n: number,
    nModified: number,
    [index: string]: any
}

export interface executeQueryArgs<O, T, F> {
  args: QuerySchemaArgs<T, F>;
  speedrun: SpeedRun<O>;
  onSuccess: Function;
  onFailure?: Function;
}
