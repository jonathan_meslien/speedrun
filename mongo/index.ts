import { SpeedRun, Pagination } from '../interfaces';
import {rejected, resolved} from '../core';
import {forge_response} from '../response';
import {QuerySchemaFunction, QuerySchemaArgs, MongooseUpdateResponse, MongooseDeleteResponse, Resource, executeQueryArgs} from './interfaces';
import {Document} from 'mongoose';

const LIMIT = 21;

const no_pagination = <T extends Resource>(resources: T[]) => {
  if (resources.length === LIMIT) {
    const before = null;
    const after = resources[LIMIT - 2]._id;
    
    return { data: resources.slice(0, LIMIT - 1), pagination: { before, after } };
  }

  return { data: resources, pagination: { before: null, after: null } };
}

const pagination_after = <T extends Resource>(resources: T[]) => {
  if (resources.length === LIMIT) {
    const before = resources[0]._id;
    const after = resources[LIMIT - 2]._id;

    return { data: resources.slice(0, LIMIT - 1), pagination: { before, after } };
  }
  
  const before = resources[0]._id;
  return { data: resources, pagination: { before, after: null } };
}

const pagination_before = <T extends Resource>(resources: T[]) => {
  if (resources.length === LIMIT) {
    const before = resources[LIMIT - 2]._id;
    const after = resources[0]._id;

    return { data: resources.slice(0, LIMIT - 1), pagination: { before, after } };
  }
  
  const after = resources[resources.length - 1]._id;
  return { data: resources, pagination: { before: null, after } };
}

export const create_pagination = <T extends Resource, O extends Pagination> (speedrun: SpeedRun<O>) => (resources: T[]) => {
  const logger = speedrun.logger;
  logger.info('create pagination >> started');

  if (resources.length > 0) {
    if (speedrun.args.after) {

      logger.info('create pagination >> succeeded');
      return pagination_after(resources); 
    }

    if (speedrun.args.before) {
      
      logger.info('create pagination >> succeeded');
      return pagination_before(resources); 
    }
    
    logger.info('create pagination >> succeeded');
    return no_pagination(resources);
  } else {

    logger.info('create pagination >> succeeded: no pagination needed');
    return resources;
  }
}

const rewrite_configurator = (identity_field: string, forbidden_fields: string[]) => (condition: any) => {
  const fields = Object.keys(condition);

  return fields.reduce((result: any, field: string) => {
    if (field === identity_field) {
      return {...result, id: condition[field] }
    }
    if (forbidden_fields.includes(field)) {
      return result;
    }
    return {...result, [field]: condition[field] } ;
  }, {});
}


export const execute_query = <O extends Pagination, T, F>(plan: QuerySchemaFunction<T,F>) => async (execute_query_args: executeQueryArgs<O, T, F>) => {
  let {args, speedrun} = execute_query_args;
  const query_schema = plan(args);
  const logger = speedrun.logger;
  let conditions = {};
  const sortType = speedrun.args.before ? -1 : 1;
  const onFailure = execute_query_args.onFailure ? execute_query_args.onFailure : default_failure;
  const onSuccess = execute_query_args.onSuccess;
  const rewrite = rewrite_configurator(query_schema.identity_field, query_schema.forbidden_fields);
  const scope_restriction = query_schema.unprotected ? {} : speedrun.policy_instruction.scope_restriction;
  if (query_schema.condition != {}) {
    conditions = { $and: [rewrite(scope_restriction), rewrite(query_schema.condition)] }; 
  } else { 
    conditions = rewrite(scope_restriction); 
  }

  logger.info(`query >> ${query_schema.name} >> started with condition : ${JSON.stringify(conditions, null, "  ")}`);
  try {
    if (query_schema.method === "FIND") { 
      const response = await query_schema.model.find(conditions).sort({ _id: sortType }).limit(LIMIT);    
      logger.info(`query >> ${plan.name} >> succeeded`);
      return onSuccess(response);

    } else if (query_schema.method === "DELETE") {
      const response = await query_schema.model.deleteOne(conditions);    

      logger.info(`query >> ${plan.name} >> succeeded`);
      return onSuccess(response);

    } else if (query_schema.method === "UPDATE") {
      if (query_schema.fields) {
        const response = await query_schema.model.updateOne(conditions, query_schema.fields);    

        logger.info(`query >> ${plan.name} >> succeeded`);
        return onSuccess(response);
      } else {
        return fields_missing();
      }

    } else if (query_schema.method === "CREATE") {
      if (query_schema.fields) {
        const Model = new query_schema.model();

        Object.keys(query_schema.fields).forEach( (key: string) => {
          Model.set(key, query_schema.fields?[key] : '');
        })

        const response = await Model.save();    

        logger.info(`query >> ${plan.name} >> succeeded`);
        return onSuccess(response);
      } else {
        return fields_missing();
      }
    }
  } catch(e) {
    logger.info(`query >> ${plan.name} >> failed >> ${JSON.stringify(e)}`);
    return onFailure();
  }
}

const default_failure = () => rejected(forge_response(500, "server error", {}));

const default_success_find = (response: Document[]) => {
  const x = extract_documents(response); 

  return resolved(x);
};

export const default_success_create = (response: Document) => resolved(extract_documents([response]));

export const default_success_update = (response: any) => is_updated(response) ? resolved('updated') : rejected('not updated');
export const default_success_delete = (response: any) => is_deleted(response) ? resolved('deleted') : rejected('not deleted');

export const fields_missing = () => rejected(forge_response(400, "fields missing", {}));

export const extract_documents = (documents: Document[]): any => {
  return documents.map(document => document.toObject());
};

export const is_updated = (document: MongooseUpdateResponse) => {
  return document.n !== 0;
}

export const is_deleted = (document: MongooseDeleteResponse) => {
  return document.deletedCount !== 0;
}





