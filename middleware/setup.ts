import {Request, Response, NextFunction} from 'express';
import { v4 as uuid } from 'uuid';
import {AppConfig, SpeedRunLog} from './interface';
import winston, { format } from 'winston';
import moment from 'moment';
import { SpeedRunRequest, Endpoint, Auths, Configuration } from '../interfaces';

const { combine } = format;

const extract_url = (url: string): string => {
  return url.split('?')[0];
}

const speedrun_log = (args: SpeedRunLog) => format((info, _) => {
    const { syslog, method, path } = args;
    info.message = `${moment().format('YYYY-MM-DD h:mm:ss a')} >> ${syslog} >> ${method} >> ${path} >> ${info.message} `;
    return info;
});

const create_logger = (log_config: SpeedRunLog) => winston.createLogger({
    level: 'info',
      format: combine(
      speedrun_log(log_config)(),
      winston.format.simple()
    ),
    transports: [
      new winston.transports.File({ filename: 'error.log', level: 'error' }),
      new winston.transports.File({ filename: 'combined.log' }),
    ],
    exitOnError: false,
  });

const extract_syslog = (req: Request) => {
  const syslog = req.headers.syslog;
  if (syslog) {
    if (Array.isArray(syslog)) {
      return syslog[0];
    }

    return syslog;
  }

  return uuid();
}

const create_speedrun_request = (logger: winston.Logger, endpoint: Endpoint, configuration: Configuration, auths?: Auths ): SpeedRunRequest => {
  return {
    logger,
    endpoint,
    configuration,
    stash: {},
    auths
  }
}

export const create_setup = (app_configuration: AppConfig) => (req: Request, res: Response, next: NextFunction) => {
  const endpoint = app_configuration.endpoints.find(endpoint => app_configuration.configuration.resource_path + endpoint.request_url === extract_url(req.originalUrl) && endpoint.request_method === req.method);
  let auths = {};
  if (endpoint) {
    const syslog = extract_syslog(req);
    const logger = create_logger({ syslog, method: endpoint.request_method, path: endpoint.request_url});
    const configuration = app_configuration.configuration;

    logger.info('setup >> -------------------');
    logger.info('setup >> endpoint found');

    if (endpoint.auths.protected) {
      if (req.headers.authorization) {
        logger.info('setup >> authorization found');
        const access_token = req.headers.authorization.split(' ')[1];
        auths = { access_token }; 
      } else {
        logger.info('setup >> failed >> authorization not found on protected endpoint');

        res.status(401);
        res.send();
        return;
      }
    }
    
    req.speedRun = create_speedrun_request(logger, endpoint, configuration, auths);
    next();
  } else {
    res.status(404);
    res.send();
    return;
  }
}
