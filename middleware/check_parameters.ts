import { Request, Response, NextFunction } from 'express';
import { checkParameters } from '../validator';

export const check_parameters = (req: Request, res: Response, next: NextFunction) => {
  const logger = req.speedRun.logger;
  logger.info('check parameters >> started');
  const validators = req.speedRun.endpoint.args;
  const result = checkParameters(validators, req); 

  if (result.code) {
    logger.info('check parameters >> failed >> ' + JSON.stringify(result.data));

    res.status(result.code);
    res.json({
      successful: false,
      message: result.message,
      data: result.data
    })
  } else {
    req.speedRun.args = result;

    logger.info('check parameters >> succeeded');
    next();
  }
}

