import {Request, Response, NextFunction} from 'express';
import {Permission, Pagination} from '../interfaces';

const has_permission = (needed_permissions: Permission[], user_permissions: Permission[]) => {
  return needed_permissions.every(permission => user_permissions.includes(permission));
}

const add_pagination = <T extends Pagination>(args: T ) => {
  if (args.after)
    return { _id: { $gt: args.after } }; 
  else if (args.before)
    return { _id: { $lt: args.before } };
  else 
    return {};
}

export const authorization = (req: Request, res: Response, next: NextFunction) => {
  const logger = req.speedRun.logger;
  const endpoint = req.speedRun.endpoint;

  logger.info('authorization >> started');
  if (req.speedRun.endpoint.auths.protected && req.speedRun.auths && req.speedRun.auths.access_token_cleared) {
    const authorization_object = req.speedRun.auths.access_token_cleared;
    const user_permissions = authorization_object.permissions;

    if(has_permission(endpoint.auths.permissions, user_permissions)) {
      try {
        req.speedRun.auths.policy_instruction = req.speedRun.endpoint.auths.policy(authorization_object);  
        const scope_restriction = req.speedRun.auths.policy_instruction.scope_restriction;
        req.speedRun.auths.policy_instruction.scope_restriction = {...scope_restriction, ...add_pagination(req.speedRun.args) };

        logger.info('authorization >> user access granted');
        next();
        return;
      } catch (e) {
        logger.error('authorization >> ' + e);
        res.status(403);
        res.send();
        return;
      }
    } else {
      logger.info('authorization >> user access not granted');
      res.status(403);
      res.send();
      return;
    }
  }
  
  logger.info('authorization >> endpoint not protected');
  next();
}
