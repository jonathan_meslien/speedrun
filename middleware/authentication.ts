import {Request, Response, NextFunction} from 'express';
import jwt from 'jsonwebtoken';
import { AuthorizationObject } from '../interfaces';

export const authentication = (req: Request, res: Response, next: NextFunction) => {
  const logger = req.speedRun.logger;

  logger.info('authentication >> started');

  if (req.speedRun.endpoint.auths.protected && req.speedRun.auths && req.speedRun.auths.access_token) {
    try {
      const access_token = req.speedRun.auths.access_token;
      const private_key = req.speedRun.configuration.jwt_access_key;
      const token_cleared = jwt.verify(access_token, private_key) as AuthorizationObject;

      req.speedRun.auths.access_token_cleared = token_cleared;

      logger.info('authentication >> succeeded');
      next();
      return;
    } catch(e) {
      logger.info('authentication >> failed ' + JSON.stringify(e, null, "  "));

      res.status(401);
      res.send();
      return;
    }

  }
  logger.info('authentication >> endpoint not protected');
  next();
}
