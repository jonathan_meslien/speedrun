import {Endpoint, Configuration, RequestMethod} from "../interfaces";

export interface AppConfig {
  endpoints: Endpoint[],
  configuration: Configuration
}

export interface SpeedRunLog {
  syslog: string,
  method: RequestMethod,
  path: string
}
