import {NextFunction, Request, Response} from "express";

export const route = (req: Request, res: Response, _: NextFunction) => {
  return req.speedRun.endpoint.method(req,res); 
}
