import {Request} from 'express';
import {_} from './operator';
import {forge_response} from './response';
import {Validator} from './interfaces';


const discardUnusedParameters = (requestParams: any, validators: Validator[]) => {
    const expectedParams = validators.map((validator) => validator.field)

    return Object.keys(requestParams).reduce( (usedParams, param): Object => {
        if (expectedParams.includes(param)) {
            return { ...usedParams, [param]: requestParams[param] };
        }

        return usedParams;
    }, {});
}

const checkRequiredParameters = (requestParams: any, validators: Validator[]) => {
    const paramsRequired = validators
        .filter((validator) => validator.required(requestParams) === true)
        .map((validator) => validator.field);

    const paramsProvided = Object.keys(requestParams);

    const missingParameters = paramsRequired.reduce((errors: string[], paramRequired: string) => {
        if (!paramsProvided.includes(paramRequired)) {
            errors.push(`Missing required parameters ${paramRequired}`);
        }

        return errors;
      }, []);

    if (missingParameters.length > 0) return missingParameters;

    return paramsProvided.reduce((errors: string[], paramProvided) => {
        const [validatorField] = validators.filter(
            (validator) => validator.field === paramProvided
        );

        if (!validatorField.predicate(requestParams[paramProvided])) {
          errors.push(validatorField.errorMsg);
        }

        return missingParameters;
      }, []);
};

const checkOptionalParameters = (requestParams: any, validators: Validator[]) => {
  const paramsProvided = Object.keys(requestParams);

  return paramsProvided.reduce((errors: string[], paramProvided) => {
    const [validatorField] = validators.filter(
      (validator) => validator.field === paramProvided
    );

    if (validatorField && !validatorField.predicate(requestParams[paramProvided])) {
      errors.push(validatorField.errorMsg);
    }

    return errors;
  }, []);
};

export const checkParameters = (validators: Validator[], req: Request): any => {
    let errors = [];
    let requestParams = { ...req.query, ...req.params, ...req.body, ...req.headers }

    const usedParams = discardUnusedParameters(requestParams, validators);

    errors = checkRequiredParameters(usedParams, validators);

    if (errors.length > 0) {
        return forge_response(400, "Bad request", errors);
    }

    errors = checkOptionalParameters(usedParams, validators);

    if (errors.length > 0) {
        return forge_response(400, "Bad request", errors);
    }

    return usedParams;
};


