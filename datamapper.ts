import { resolved } from "./core";
import {_} from "./operator";
import { SpeedRun } from "./interfaces";

interface mapping {
    source: string,
    target: string,
    transform?: Function
}

export const TO_SHRINK = 'TO_SHRINK';


export const mapFields = <T>(mapping: mapping[], speedrun: SpeedRun<T>) => (data: any) => {
    console.log('ici');
    const logger = speedrun.logger;

    const isPaginated = data.hasOwnProperty('data') && data.hasOwnProperty('pagination'); 
    const resources = isPaginated ? data.data : data as any[];    

    const fields_to_shrink = speedrun.policy_instruction.fields_to_shrink;
    logger.info('map fields >> started')

    const dataMapped = resources.map((entry: any) => {
      const fields = Object.keys(entry);
      const final_mapping = [...fields_to_shrink(entry), ...mapping];

      return fields.reduce((entryMapped, field) => {
          const [mapper] = final_mapping.filter((item) => item.source === field);

          if (mapper === undefined) {
              return { ...entryMapped, [field]: entry[field] };
          }

          if (mapper.transform !== undefined && mapper.target != TO_SHRINK) { 
              return {
                ...entryMapped,
                [mapper.target]: mapper.transform(entry[field], entry),
              };
          }

          if (mapper.target === TO_SHRINK) {
              return entryMapped;
          }

          return { ...entryMapped, [mapper.target]: entry[field] };
     
      }, {});
    });

    logger.info('map fields >> succeeded')
    return isPaginated ? resolved({data: dataMapped, pagination: data.pagination}) : resolved(dataMapped);
};

export const map_fields = (mapping: mapping[]) => _(<O>(speedrun: SpeedRun<O>) => mapFields(mapping, speedrun));

