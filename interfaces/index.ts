import {Request, Response} from "express";
import winston from "winston";

type EndpointMethod = (req: Request, res: Response) => void;
type PredicateFunction = (value: any) => Boolean;

export type RequestMethod = "GET" | "POST" | "PUT" | "DELETE";

export interface Validator {
  field: string,
  predicate: PredicateFunction,
  errorMsg: string,
  required: (request_parameters?: any) => Boolean
}

interface MapField {
  source: string,
  target: string,
  transform?: Function
}

export interface PolicyInstruction {
  fields_to_shrink: (data: any) => MapField[],
  scope_restriction: {};
  user_authorization: AuthorizationObject;
}

export type Permission = "CREATE_USER" | "GET_USER";

export type PolicyFunction = (authorization_object: AuthorizationObject) => PolicyInstruction

export interface Auths {
  access_token?: string,
  access_token_cleared?: AuthorizationObject,
  policy_instruction?: PolicyInstruction
}

export interface Pagination {
  after?: string,
  before?: string
}

export interface SpeedRunRequest {
  logger: winston.Logger,
  endpoint: Endpoint,
  configuration: Configuration,
  stash: any,
  args?: any,
  auths?: Auths
}

export interface AuthorizationObject {
  permissions: Permission[];
    user_id: string;
    mail: string;
    tower_id: string;
    society_id: string;
    realms: {
        [key: string]: any;
    };
    clearance_level: number;
    role_name: string;
}

export interface Endpoint {
  request_url: string,
  request_method: RequestMethod,
  method: EndpointMethod,
  args: Validator[],
  auths: {
    protected: boolean,
    permissions: Permission[],
    policy: PolicyFunction
  }
}

export interface Configuration {
  node_env: "DEV" | "PROD",
  jwt_access_key: string,
  jwt_refresh_key: string,
  resource_path: string,
  [key: string]: any
}

export interface SpeedRun<T> {
  args: T,
  policy_instruction: PolicyInstruction,
  logger: winston.Logger,
  stash: any
}