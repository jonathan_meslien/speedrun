import { _ } from './operator';
import { PolicyInstruction, SpeedRun } from './interfaces';
import winston from 'winston';

export const resolved = (data: any) => Promise.resolve(data);
export const rejected = (data: any) => Promise.reject(data);

const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/gm;
const ARGUMENT_NAMES = /([^\s,]+)/g;

// const inject_speedrun_request = (speed_run: SpeedRun<any>, fcts: Function[]) => {
//   return fcts.map(fct => {
//     if (need_speedRun_request(fct)) {

//       const res = fct(speed_run);

//       if (res.left) {
//         return res;
//       } else {
//         return _(res);
//       }

//     } else if ( need_speedRun_logger(fct)) {

//       const res = fct(speed_run.logger);

//       if (res.left) {
//         return res;
//       } else {
//         return _(res);
//       }
//     }

//     return _(fct);
//   })
// }

// export const feature = <T>(...fcts: Function[]) => (speed_run: SpeedRun<T>) => {
//     const fcts_ready_to_execute = inject_speedrun_request(speed_run, fcts);
//     return fcts_ready_to_execute.reduceRight((chain, operator) => {
//         return chain.then(operator.left).catch(operator.right);
//     }, Promise.resolve(speed_run.args));
// }

export const feature = <T>(...operators: Function[]) => (speedrun: SpeedRun<T>) => {
  const fcts = operators.map((operator) => operator(speedrun));
  return fcts.reduceRight((chain, operator) => {
      return chain.then(operator.left).catch(operator.right);
  }, Promise.resolve(speedrun.args));
}

// const need_speedRun_request = (func: Function) => {
//   const [nextArg] = getParamNames(func);

//   return nextArg === "speedRun";
// };

// const need_speedRun_logger = (func: Function) => {
//   const [nextArg] = getParamNames(func);

//   return nextArg === "logger";
// };

//  const getParamNames = (func: Function) => {
//     const fnStr = func.toString().replace(STRIP_COMMENTS, "");
//     let result = fnStr
//     .slice(fnStr.indexOf("(") + 1, fnStr.indexOf(")"))
//     .match(ARGUMENT_NAMES);

//   if (result === null) result = [];
//   return result;
// };

const empty_policy_instruction: PolicyInstruction = ({
  fields_to_shrink: () => [],  
  scope_restriction: {},
  user_authorization: {
    permissions: [],
    user_id: '',
    mail: '',
    tower_id: '',
    society_id: '',
    realms: {},
    clearance_level: -1,
    role_name: ''
  }
})

export const create_speedRun = <T>(args: T, logger: winston.Logger, policy_instruction?: PolicyInstruction | undefined) => {
  if (policy_instruction)
    return { args, logger, policy_instruction, stash: {} };

  return { args, logger, policy_instruction: empty_policy_instruction, stash: {}}
}
