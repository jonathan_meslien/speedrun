import { SpeedRun } from './interfaces';
import { rejected } from "./core"

export interface operator {
    left: any,
    right: any
}

const defaultBehaviour = <O>(speedrun: SpeedRun<O>) => (data: any) => rejected(data);

export const _ = (leftFct: Function, rightFct?: Function | undefined) => <O>(speedrun: SpeedRun<O>): operator => {
    const right = rightFct ? rightFct(speedrun) : defaultBehaviour(speedrun);
    const left = leftFct(speedrun);
    return { left, right }
}
